const dataPenjualanNovel = [
    {
      idProduct: 'BOOK002421',
      namaProduk: 'Pulang - Pergi',
      penulis: 'Tere Liye',
      hargaBeli: 60000,
      hargaJual: 86000,
      totalTerjual: 150,
      sisaStok: 17,
    },
    {
      idProduct: 'BOOK002351',
      namaProduk: 'Selamat Tinggal',
      penulis: 'Tere Liye',
      hargaBeli: 75000,
      hargaJual: 103000,
      totalTerjual: 171,
      sisaStok: 20,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Garis Waktu',
      penulis: 'Fiersa Besari',
      hargaBeli: 67000,
      hargaJual: 99000,
      totalTerjual: 213,
      sisaStok: 5,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Laskar Pelangi',
      penulis: 'Andrea Hirata',
      hargaBeli: 55000,
      hargaJual: 68000,
      totalTerjual: 20,
      sisaStok: 56,
    },
  ];
  
  function changeFormat(data){
    return new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR", minimumFractionDigits: 0 }).format(data)
  }
  
  function totalKeuntungan(data){
            let h_jual = data.map(a => a.hargaJual)
        let h_beli = data.map(a => a.hargaBeli)
            let h_terjual = data.map(a => a.totalTerjual)
            let total = 0
        for (let i = 0; i < data.length; i++) {
                  total += ( h_jual[i] - h_beli[i] ) * h_terjual[i] 
              }
            result.totalKeuntungan = changeFormat(total)
        return total
  }
  
  function totalModal(data){
    
    let h_beli = data.map(a => a.hargaBeli);
    let sisaStok = data.map(a => a.sisaStok);
    let totalTerjual = data.map(a => a.totalTerjual);
  
    let total = []
    for (let i = 0; i < data.length ; i++) {
              total.push((sisaStok[i] + totalTerjual[i]) * h_beli[i])
          } 
    let totalModal = 0
          for (let i = 0; i < data.length; i++) {
                    totalModal += total[i];
                }
    result.totalModal = changeFormat(totalModal)
    return totalModal
  }
  
  function produkBukuTerlaris(data){
    let terjual = data.map(a => a.totalTerjual);
    terjual.sort(function(a, b){return a - b});
    let nTertinggi = terjual[terjual.length-1]
    data.forEach((element) => {
        if (element.totalTerjual == nTertinggi) {
          tempBukuTerlaris = element.namaProduk;
        }
      })
    result.produkBukuTerlaris = tempBukuTerlaris
  }
  
  
  function presentaseKeuntungan(data) {
      let t_Modal = totalModal(data)
      let t_Keuntungan = totalKeuntungan(data)
      let persen = (t_Keuntungan / t_Modal) * 100 + " %"
      result.persentaseKeuntungan = persen;
        return persen
    }
  
  function penulisTerlaris(data){
    let terjual = data.map(a => a.totalTerjual);
    terjual.sort(function(a, b){return a - b});
    let nTertinggi = terjual[terjual.length-1]
    data.forEach((element) => {
        if (element.totalTerjual == nTertinggi) {
          penulisTerlaris = element.penulis;
        }
      })
    result.penulisTerlaris = penulisTerlaris	
  }
  
  const result = {
      totalKeuntungan: "",
      totalModal: "",
      persentaseKeuntungan: "",
      produkBukuTerlaris: "",
      penulisTerlaris: "",
    };
    
  
  function getInfoPenjualan(dataPenjualan) {
      if (typeof(dataPenjualan) == 'object') {
        totalKeuntungan(dataPenjualan)
        totalModal(dataPenjualan)
        produkBukuTerlaris(dataPenjualan)
              presentaseKeuntungan(dataPenjualanNovel)
              penulisTerlaris(dataPenjualanNovel)
        return result
      } else {
            console.log('Data Bukan Object')
      }
    }
  
  
console.log(getInfoPenjualan(dataPenjualanNovel))
  